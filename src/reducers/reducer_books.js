
export default function() {
    return [
        { title: 'The Prince', pages: 1203 },
        { title: 'ISIS Papers', pages: 784 },
        { title: 'The Art of War', pages: 2650 },
        { title: '48 Laws of Power', pages: 500 }
    ];
}
