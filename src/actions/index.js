// An action creator function
export function selectBook(book) {
    console.log('A book has been selected from the action creator.');
    console.log(book);

    // selectBook is an ActionCreator, it needs to return an action,
    // an object with a type property.
    return {
        type: 'BOOK_SELECTED',
        payload: book
    };
}
